/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication. (use return statement)

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division. (use return statement)

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided radius.
			-a number should be provided as an argument.
			-formula: area = pi * (radius raised to two)
			-look up the use of the exponent operator. (ES6 update)
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation. (use return statement)

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

		Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-formula for average: num1 + num2 + num3 + ... divide by total numbers
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation. (use return statement) 

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

// 1st item
// Addition

function sum_TwoNumbers (first_number, second_number){

	let sum = first_number + second_number;
	console.log("The sum of " + first_number + " and " + second_number + " is = " + sum);
};

sum_TwoNumbers(4, 2);

// Subtraction

function diff_TwoNumbers (first_number, second_number){

	let diff = first_number - second_number;
	console.log("The difference of " + first_number + " and " + second_number + " is = " + diff)

};

diff_TwoNumbers(4, 2);

// 2nd item
// multiplication

function product_TwoNumbers (first_number, second_number){

	console.log("The product of " + first_number + " and " + second_number + " is: ");

	let product_Equation = first_number * second_number;
	return product_Equation;
};

let product_Answer = product_TwoNumbers(5, 5);
console.log(product_Answer);

// Division

function quotient_TwoNumbers (first_number, second_number){

	console.log("The quotient of " + first_number + " and " + second_number + " is: ");

	let quotient_Equation = first_number / second_number;
	return quotient_Equation;
};

let quotient_Answer = quotient_TwoNumbers(25, 5);
console.log(quotient_Answer);

// 3rd item

function area_Circle(radius){

	console.log("The area of a circle with a radius of " + radius + " is equal to:");

	return Math.PI * (radius**2);
};

let area_Answer = area_Circle(5);
console.log(area_Answer);

// 4th item

function total_average (first_number, second_number, third_number, fourth_number){

	console.log("The average of the numbers " + first_number + ", " + second_number + ", "+ third_number + " and, "+ fourth_number + " " + "is equal to:");

	let average_formula = (first_number + second_number + third_number + fourth_number) / 4;

	return average_formula;
};

let average_answer = total_average(1,2,3,4);
console.log(average_answer);

// 5th item

function percentage_Calculate(my_Score, total_Score){

	console.log("Test result: " + my_Score +"/" + total_Score);

	let percentage_formula = (my_Score/total_Score) * 100;

	let isPassed = percentage_formula>total_Score

	return isPassed

};

let percentage_Score = percentage_Calculate(30,40);
console.log("Pass: " + percentage_Score);
